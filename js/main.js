//=========================
//= MODIFIABLE PARAMETERS =
//=========================

// A list of requirement types that should solely be present at each level
// For this example:
// Level 1:   Mission requirements
// Level 2:   System requirements
// Level 3:   System requirements that are children of other system requirements
// Levels 4+: All other requirements, or children of requirements in level 3
const levels = [
  [ 'ASAT' ],
  [ 'ASAT' ],
  [ 'ASAT' ],
  [ 'SYS', 'FDS', 'ASAT' ],
  [ 'SYS' ],
  [ 'PAY', 'SYS' ],
  [ 'PAY' ],
];

// Keywords to search for in .csv file column titles for requirements (case insensitive)
// For this example:
// A column with a title that contains ID (e.g. "Req Id") is considered to be the ID column
const columnKeywords = {
  id: "ID",
  text: "Text",
  parent: "Parent"
}

// Width of the requirements tree, modify to set letter sizing
const width = 1500;

//====================
//= APPLICATION CODE =
//====================

const render = function() {
  const $canvas = document.getElementById('canvas');

  // Function to split the requirements into different levels horizontally, as defined
  // in the levels variable
  var levelise = function(data, level = 0) {
    if (levels[level] === undefined) {
      return;
    }

    for (r in data.children) {
      const requirement = data.children[r];

      // Get requirement group/subsystem/discipline
      if (requirement.name === undefined) {
        continue
      }

      if (levels[level].length) {
        // This level is restricted, reject some requirements
        let found = false;
        for (let discipline of levels[level]) {
          if (requirement.name.startsWith(discipline)) {
            found = true;
            break;
          }
        }

        if (!found) {
          // Not the correct discipline, push it down one more
          data.children[r] = {
            children: [ requirement ]
          }
        }
      }

      levelise(data.children[r], level + 1);
    }
    return data;
  }

  var tree = data => {
    const root = d3.hierarchy(data);
    root.dx = 10;
    root.dy = width / (root.height + 1);

    // Separation space based on whether requirements are for the same discipline
    // Note: Assuming requirements of the format XXX-YYY (where YYY can contain other dashes as well)
    return d3.tree().separation(function(a,b) {
        if (a.data.name === undefined || b.data.name === undefined) return 1;
        return a.data.name.split('-')[0] == b.data.name.split('-')[0] ? 1 : 2;
    }).nodeSize([root.dx, root.dy])(root);
  }

  var dendrogram = data => {
    const root = d3.hierarchy(data).sort((a, b) => d3.descending(a.height, b.height) || d3.ascending(a.data.name, b.data.name));
      root.dx = 10;
      root.dy = width / (root.height + 1);
    return d3.cluster().nodeSize([root.dx, root.dy])(root);
  }

  var chart = function(tree) {
    const root = tree;

    let x0 = Infinity;
    let x1 = -x0;
    root.each(d => {
      if (d.x > x1) x1 = d.x;
      if (d.x < x0) x0 = d.x;
    });

    const svg = d3.create("svg")
        .attr("viewBox", [0, 0, width, x1 - x0 + root.dx * 2])
        .attr("class", "tree")

    const g = svg.append("g")
        .attr("font-family", "sans-serif")
        .attr("font-size", 10)
        .attr("transform", `translate(${- root.dy / 3},${root.dx - x0})`);

    const link = g.append("g")
      .attr("fill", "none")
      .attr("stroke", "#555")
      .attr("stroke-opacity", 0.4)
      .attr("stroke-width", 1.5)
    .selectAll("path")
      .data(root.links().filter(function(l) {
        // Don't show parent links to parentless requirements
        var source = l.source;
        while (source) {
          // Requirements without a name are not considered
          // existent
          if (source.data.name !== undefined) {
            return true;
          }
          source = source.parent;
        }

        return false;
      }))
      .join("path")
        .attr("d", d3.linkHorizontal()
            .x(d => d.y)
            .y(d => d.x));

    const node = g.append("g")
        .attr("stroke-linejoin", "round")
        .attr("stroke-width", 3)
      .selectAll("g")
      .data(root.descendants())
      .join("g")
        .attr("transform", d => `translate(${d.y},${d.x})`)
        .each(function(requirement) {
          if (requirement.data.value) {
            // Requirement pop-up menu
            var content = document.createElement("div");

            var title = document.createElement("em");
            title.innerText = requirement.data.name;
            content.appendChild(title);
            content.append(`: ${requirement.data.value.text}`)

            tippy(this, {
              //TODO: Escape HTML
              allowHTML: true,
              content: content,
              // interactive: true,
              // interactiveBorder: 5,
              appendTo: () => document.body,
            })
          }
        });

    node.filter(d => d.data.name)
        .append("circle")
        .attr("fill", d => d.children ? "#555" : "#999")
        .attr("r", 2.5);

    node.append("text")
        .attr("dy", "0.31em")
        .attr("x", d => d.children || d.depth <= levels.length ? -6 : 6)
        .attr("text-anchor", d => d.children || d.depth <= levels.length ? "end" : "start")
        .each(function(requirement) {
            if (requirement.data.value != undefined && requirement.data.value.external) {
                // Give a less distinct look to external requirements
                this.style.fill = '#aaa';
            }
        })
        .text(d => d.data.name)
      .clone(true).lower()
        .attr("stroke", "white");

    return svg.node();
  }

  d3.csv("./requirements.csv").then(function(data) {
    reqTree = [];
    reqList = {};

    // First, gather all requirement column titles
    var columnNames = {};
    for (requirement of Object.values(data)) {
      for (column in requirement) {
        for (let [attribute, keyword] of Object.entries(columnKeywords)) {
          if (column.toLowerCase().includes(keyword.toLowerCase())) {
            console.log(`Using column ${column} for ${keyword}`)
            columnNames[attribute] = column;
            break;
          }
        }
      }
      break; // First object is enough
    }

    // Pass 1: Add requirements to list
    for (requirement of Object.values(data)) {
      // Translate CSV attributes into wanted attributes
      for (let [attribute, column] of Object.entries(columnNames)) {
        requirement[attribute] = requirement[column];
      }

      requirement.external = false;

      const id = requirement.id;
      const parent = requirement.parent;

      if (id === "") {
        // Requirement does not exist
        continue;
      }

      if (!/\d/.test(id)) {
        // The requirement ID does not contain a number
        continue;
      }

      if (requirement.text.toLowerCase() == 'deleted') {
        continue;
      }

      if (requirement.parent.toLowerCase() == 'none') {
        requirement.parent = '';
      }

      reqList[id] = {
        name: id,
        children: [],
        value: requirement
      }
    }

    // Pass 2: Add children requirements as tree
    for (requirement of Object.values(reqList)) {
      const id = requirement.value.id;
      const parent = requirement.value.parent;

      if (parent !== "" && reqList[parent] !== undefined) {
        reqList[parent].children.push(reqList[id]);
      } else if (parent !== "") {
        // This requirement has a parent, but the parent does not exist in the specification.
        // It is an external parent. Create this parent as a new requirement
        reqList[parent] = {
          name: parent,
          children: [ reqList[id] ], // the only child is the current requirement
          value: { id: parent, text: "Unspecified", external: true } // parent has no value :(
        }
        reqTree.push(reqList[parent]); // external parents are orphans
      } else {
        // This requirement does not have a parent; make it an orphan
        reqTree.push(reqList[id]);
      }
    }

    visData = {
      children: reqTree
    };

    if (window.location.hash == "#children") {
      for (requirement of Object.values(reqList)) {
        let list = document.createElement("dl");
        list.classList.add('requirement');

        let title = document.createElement("dt");
        let content = document.createElement("dd");

        const children = requirement.children.map(c => c.value.id).join(', ');

        title.appendChild(document.createTextNode(requirement.value.id));
        content.appendChild(document.createTextNode(children));

        list.appendChild(title);
        list.appendChild(content);

        $canvas.appendChild(list);
      }
    } else if (window.location.hash == "#leaf") {
      $canvas.appendChild(chart(dendrogram(visData)));
    } else if (window.location.hash == "#unlevel") {
      $canvas.appendChild(chart(tree(visData)));
    } else {
      levelise(visData);
      $canvas.appendChild(chart(tree(visData)));
    }
  });

};

render();

window.addEventListener('hashchange', function(){
  document.getElementById('canvas').innerHTML = '';
  render();
})

window.addEventListener('load', function() {
  document.getElementById('js--project-name').textContent = config['project'];

  $footer = document.getElementById('js--disclaimer');
  for (const logo of config['logos']) {
    $img = document.createElement("img");
    let attr;

    attr = document.createAttribute('src')
    attr.value = logo;
    $img.attributes.setNamedItem(attr);

    attr = document.createAttribute('alt')
    attr.value = "Logo";
    $img.attributes.setNamedItem(attr);

    $footer.appendChild($img);
  }

  let disclaimer = document.createElement('div');
  disclaimer.innerHTML = config['disclaimer'];
  $footer.appendChild(disclaimer);
})
