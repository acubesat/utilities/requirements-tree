var config = {
  project: 'NanoSaT',
  logos: [
    'logos/nanosat_logo.png',
    'logos/university_logo.png'
  ],
  disclaimer:
    '<p>The NanoSaT project is carried out with support from the <a href="https://example.com/">XYZ project</a></p>'
}
